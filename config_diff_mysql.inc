<?php

/**
 * _config_diff_table_compare_make_mysql()
 *
 * Return an array of SQL statements to make the local
 * database table structure match that of the remote.
 *
 * Uses MySQL specific SQL.
 */
function _config_diff_table_compare_make_mysql($table_name) {

  $sql = array();

  // Switch to remote database.
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);

  // Get the table construct.
  $r = db_query('SHOW CREATE TABLE {'. $table_name .'}');
  $row = db_fetch_array($r);
  $sql[] = $row['Create Table'];

  // Get the field types.
  $r = db_query('DESCRIBE {'. $table_name .'}');
  while ($row = db_fetch_array($r)) {
    $field_types[$row['Field']] = $row['Type'];
  }

  // Get the data to populate it.
  $r = db_query('SELECT * FROM {'. $table_name .'}');
  while ($row = db_fetch_array($r)) {
    $fields = array_keys($row); array_walk($fields, 'db_escape_string');
    $values = _config_diff_mysql_escape_array($row, $field_types);
    $sql[] = 'INSERT INTO {'. $table_name .'} ('. implode('`, `', $fields) .'`) VALUES ('. implode(',', $values) .')';
  }
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);

  // Return the list of SQL statements.
  return $sql;
}

/**
 * _config_diff_forward_port_table()
 */
function _config_diff_forward_port_table($table_name) {

  // Prepare return array.
  $sql = array();

  // Switch to query remote database.
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);

  // Get the table construct.
  $r = db_query("SHOW CREATE TABLE {". $table_name ."}");
  $row = db_fetch_array($r);
  $create_sql = $row['Create Table'];
  $r = db_query("SELECT * FROM {". $table_name ."}");
  while ($row = db_fetch_array($r)) {
    $rows[] = $row;
  }

  // Get the field types.
  $r = db_query('DESCRIBE {'. $table_name .'}');
  while ($row = db_fetch_array($r)) {
    $field_types[$row['Field']] = $row['Type'];
  }

  // Get the data to populate it.
  $inserts = array();
  $r = db_query('SELECT * FROM {'. $table_name .'}');
  while ($row = db_fetch_array($r)) {
    $fields = array_keys($row); array_walk($fields, 'db_escape_string');
    $values = _config_diff_mysql_escape_array($row, $field_types);
    $inserts[] = 'INSERT INTO {'. $table_name .'} (`'. implode('`, `', $fields) .'`) VALUES ('. implode(',', $values) .')';
  }

  // Switch back to local database.
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);

  // Begin build queries to effect the move.
  $sql[] = "RENAME TABLE {". $table_name ."} TO {". md5($table_name) ."}";
  $sql[] = $create_sql;
  $sql = array_merge($sql, $inserts);
  $sql[] = "DROP TABLE {". md5($table_name) ."}";

  // Return those queries to caller.
  return $sql;
}


/**
 * _config_diff_mysql_escape_array()
 *
 * Provided an array of field/value pairs iterate over them
 * and apply database escaping depending upon the field type.
 */
function _config_diff_mysql_escape_array($row, $field_types) {
  $values = array();
  foreach ($row as $field => $value) {
    $brace = strpos($field_types[$field], '(');
    $type = $brace === FALSE ? $field_types[$field] : substr($field_types[$field], 0, $brace);

    switch (strtolower($type)) {
      case 'int':
      case 'bit':
      case 'tinybit':
      case 'bool':
      case 'boolean':
      case 'tinyint':
      case 'smallint':
      case 'mediumint':
      case 'bigint':
        $values[] = (int)$value;
        break;
      case 'varchar':
      case 'char':
      case 'text':
      case 'mediumtext':
      case 'longtext':
      case 'date':
      case 'datetime':
      case 'timestamp':
      case 'time':
      case 'year':
      case 'tinytext':
        $values[] = "'". db_escape_string($value) ."'";
        break;
      case 'blob':
      case 'tinyblob':
      case 'varbinary':
      case 'mediumblob':
      case 'longblob':
        $values[] = db_encode_blob($value);
        break;
      case 'float':
      case 'double':
        $values[] = (float)($value);
        break;
    }

  }
  return $values;
}
