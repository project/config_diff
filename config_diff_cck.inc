<?php




function _config_diff_cck_anaylize ($args = array()) {

    // Get the local node types.
  $local_node_types = _config_diff_get_node_types();

  // Get the remote node types.
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);
  $remote_node_types = _config_diff_get_node_types();
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);

  // Analyse the node types from each database.
  $output = _config_diff_cck_node_types_analyse($local_node_types, $remote_node_types);

  return
    $output .
    "<p />".
    "Local:<pre>". print_r($local_node_types, TRUE) ."</pre>" .
    "Remote:<pre>". print_r($remote_node_types, TRUE) ."</pre>"
  ;
}

/**
 * _config_diff_cck_node_types_analyse()
 *
 * The is the workhorse function. It digs into each node type
 * and analyses each type looking for alterations to be made.
 *
 * @param $local array
 *   An structured array of the {node_type} table in this Drupal instance.
 * @param $remote array
 *   An structured array of the {node_type} table on the remote dev Drupal instance.
 * @return string
 *   Render html analysis of the findings and buttons to instigate changes.
 */
function _config_diff_cck_node_types_analyse($local, $remote) {

  // drupal_add_css(drupal_get_path('module', 'config_diff') .'/config_diff.css');

  // Prepare return string for concat.
  $output = '';

  // Check we have data about the remote node_type table.
  if (!is_array($remote) || !count($remote)) {
    $output[] = t('No node types found in remote database');
  }

  // Check we have data about the local node_type table.
  if (!is_array($local) || !count($local)) {
    $output[] = t('No node types found in local database');
  }

  // If an error occurs $output becomes an array of error messages
  // rather than a return string. Set messages and return problem.
  if (is_array($output) && count($output)) {
    foreach ($output as $message) {
      drupal_set_message($message, 'error');
    }
    return t('There was a problem getting information about your node type structures.');
  }

  // Loop through the data structure of the remote node_type table and analyse.
  foreach ($remote as $type => $data) {

    // Create a shortcut variable into the local node_type structure.
    $local_data = isset($local[$type]) ? $local[$type] : FALSE;

    // Prepare to start building an html table of results.
    $rows = array();

    // If the type does not exist in the local database display notice.
    if (!$local_data) {
      $row = array();
      $row['data'][] = array('data' => t('New node type @type', array('@type' => $type)), 'colspan' => '2');
      $rows[] = $row;
      $sqls = _config_diff_cck_make_new_node_type($type, $data);
      foreach ($sqls as $sql) {
        $message = '<span class="error">[ '. $sql .' ]</span><br />';
        $row = array('class' => 'even');
        $row['data'][] = array('data' => $message, 'colspan' => '2');
        $rows[] = $row;
      }
    }
    else {
      list($same, $new, $old) = _config_diff_array_compare($data, $local_data);
      if ($same) {
        $row = array();
        $row['data'][] = array('data' => t('Local and remote node type is the same'), 'colspan' => '2');
        $rows[] = $row;
      }
      else {
        $row = array();
        $row['data'][] = array('data' => t('Differences were detected'), 'colspan' => '2');
        $rows[] = $row;
        foreach ($old as $field_name => $field_value) {
          if (!isset($new[$field_name])) {
            $sql = _config_diff_cck_make_sql_node_type($type, $field_name, $new, $old);
            $row = array('class' => 'even');
            $row['data'][] = array('data' => '<strong>'. check_plain($field_name) .'</strong>', 'valign' => 'top');
            $row['data'][] = array('data' => t('Old field needs to be dropped') ." <span class=\"error\">[ $sql ]</span>", 'valign' => 'top');
            $rows[] = $row;
          }
        }

        foreach ($new as $field_name => $field_value) {
          if (isset($old[$field_name])) {
            $sql = _config_diff_cck_make_sql_node_type($type, $field_name, $new, $old);
            $row = array('class' => 'even');
            $row['data'][] = array('data' => '<strong>'. check_plain($field_name) .'</strong>', 'valign' => 'top', 'rowspan' => 3);
            $row['data'][] = array('data' => '<strong><em>'.t('was:') .' </em></strong>'. check_plain($old[$field_name]), 'valign' => 'top');
            $rows[] = $row;
            $row = array('class' => 'even');
            $row['data'][] = array('data' => '<strong><em>'.t('is now:') .' </em></strong>'. check_plain($field_value), 'valign' => 'top');
            $rows[] = $row;
            $row = array('class' => 'even');
            $row['data'][] = array('data' => '<span class="error">[ '. check_plain($sql) .' ]</span>', 'valign' => 'top');
            $rows[] = $row;
          }
          else {
            $sql = _config_diff_cck_make_sql_node_type($type, $field_name, $new, $old);
            $row = array('class' => 'even');
            $row['data'][] = array('data' => '<strong>'. check_plain($field_name) .'</strong>', 'valign' => 'top');
            $row['data'][] = array('data' => t('New field needs to be added') ." <span class=\"error\">[ $sql ]</div>", 'valign' => 'top');
            $rows[] = $row;
          }

        }
      }
    }

    $header['data'] = array('data' => t('Analyse %type', array('%type' => $type)), 'colspan' => '2');

    $output .= theme('table', $header, $rows);
  }

  // We now get back all the stored SQL statements. Note, we have
  // to create a dummy variable as two are passed by reference so
  // NULL doesn't work for doing that.
  $dummy = array();
  $sql = _config_diff_cck_make_sql_node_type(FALSE, NULL, $dummy, $dummy);

  // Not strictly needed but nice to keep the
  // actual SQL in the value side of the array.
  //if (count($sql)) {
  //  $sql = array_flip($sql);
  //}

  return $output . "<pre>" . print_r($sql, TRUE) ."</pre>";
}

/**
 * _config_diff_get_node_types()
 */
function _config_diff_get_node_types() {

  $r = db_query("SELECT * FROM {node_type}");

  $rows = array();
  if ($r) {
    while ($row = db_fetch_array($r)) {
      $rows[$row['type']] = $row;
    }
  }
  return $rows;
}

function _config_diff_array_compare($local, $remote) {

  if (!is_array($local) || !is_array($remote)){
    drupal_set_message(t('Erm, this should not happen!', 'error'));
    return FALSE;
  }

  $old = array_diff_assoc($remote, $local);
  $new = array_diff_assoc($local, $remote);

  return array((bool)(count($old) == 0 && count($new) == 0), $new, $old,);
}

function _config_diff_cck_make_sql_node_type($type, $field_name, &$new, &$old) {
  global $db_type;
  static $stored_sql = array();
  static $sql_counter = 0;

  if ($type === FALSE) {
    return $stored_sql;
  }

  // We need to decide if an "update" to the field's value
  // is required or whether a field needs to be added.

  $sql = FALSE;

  $field = db_escape_string($field_name);

  if (isset($new[$field_name]) && isset($old[$field_name])) {
    // The field exists in both the old and the new. In that
    // case we need to update the field.
    $value = db_escape_string($new[$field_name]);
    $sql = "UPDATE {node_type} SET $field = '$value' WHERE type = '". db_escape_string($type) ."'";
  }

  else if (isset($new[$field_name]) && !isset($old[$field_name])) {
    // The value is a new field to add into the table.
    // We need to get a description of the field type
    // from the remote database in order to build an
    // SQL ALTER TABLE statement. Postgres users are
    // more than welcome to provide patches here ;)
    switch ($db_type) {
      case 'mysql':
      case 'mysqli':
        _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);
        $r = db_query("DESCRIBE {node_type}");
        while ($row = db_fetch_array($r)) {
          if ($row['Field'] == $field_name) {
            $field_type = $row['Type'];
            $null = $row['Null'] == 'YES' ? '' : 'NOT NULL';
            $default = strlen($row['Default']) ? "default '{$row['Default']}'" : '';
            // No escape required here as the variables don't come from
            // user input but are machine read from the remote database.
            $sql = "ALTER TABLE {node_type} ADD ($field $field_type $null $default)";
          }
        }
        _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);
        break;
      case 'pgsql':
        // Expert Postgres users are invited to submit patches
        // as I know bugger all about Postgres to do what I
        // want here.
        return FALSE;
        break;
    }
  }

  else if (!isset($new[$field_name]) && isset($old[$field_name])) {
    // The value is a new field to add into the table.
    // We need to get a description of the field type
    // from the remote database in order to build an
    // SQL ALTER TABLE statement. Postgres users are
    // more than welcome to provide patches here ;)
    switch ($db_type) {
      case 'mysql':
      case 'mysqli':
        $r = db_query("DESCRIBE {node_type}");
        while ($row = db_fetch_array($r)) {
          if ($row['Field'] == $field_name) {
            $field_type = $row['Type'];
            $null = $row['Null'] == 'YES' ? '' : 'NOT NULL';
            $default = strlen($row['Default']) ? "default '{$row['Default']}'" : '';
            // No escape required here as the variables don't come from
            // user input but are machine read from the remote database.
            $sql = "ALTER TABLE {node_type} DROP $field";
          }
        }
        break;
      case 'pgsql':
        // Expert Postgres users are invited to submit patches
        // as I know bugger all about Postgres to do what I
        // want here.
        return FALSE;
        break;
    }
  }

  // Save this for a future call to get SQL values.
  $stored_sql[$type][$field_name][$sql] = $sql_counter++;

  return $sql;
}

function _config_diff_cck_make_new_node_type($node_type, $node_data) {
  global $db_type;

  // The $node_type string should be machine readable already but to
  // ensure nothing leaks into the SQL handle it securely anyway.
  $safe_node_type = db_escape_table($node_type);

    switch ($db_type) {
      case 'mysql':
      case 'mysqli':
        // Begin by generating SQL to populate Drupal's node_type table.
        $fields = array_keys($node_data); array_walk($fields, 'db_escape_string');
        $values = $node_data;             array_walk($values, 'db_escape_string');
        $sql[] = "INSERT INTO {node_type} (`". implode('`, `', $fields) .'`) VALUES (\''. implode("','", $values) .'\')';

        // Check to see if the content_type_xxx exists in the remote but not in the local.
        $local_content_type_exists = db_table_exists("{content_type_". $safe_node_type ."}");
        _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);
        $remote_content_type_exists = db_table_exists("{content_type_". $safe_node_type ."}");
        _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);

        if ($remote_content_type_exists && !$local_content_type_exists) {
          // We need to generate SQL to create in the local database content_type_xxx table...
          _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);
          $r = db_query("SHOW CREATE TABLE {content_type_". $safe_node_type ."}");
          $row = db_fetch_array($r);
          $sql[] = $row['Create Table'];
          // ...and then populate if required.
          $r = db_query("SELECT * FROM {content_type_". $safe_node_type ."}");
          while ($row = db_fetch_array($r)) {
            $fields = array_keys($row); array_walk($fields, 'db_escape_string');
            $values = $row;             array_walk($values, 'db_escape_string');
            $sql[] =
              "INSERT INTO {content_type_". $safe_node_type .'} ('.
              "`". implode("`, `", $fields) ."`) VALUES (".
              "'". implode("', '", $values) ."')";
          }
          _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);
        }
        else if($remote_content_type_exists && $local_content_type_exists) {
          // Need to check for differences between the two and generate SQL to amend the local database.

        }

        break;
      case 'pgsql':
        // Expert Postgres users are invited to submit patches
        // as I know bugger all about Postgres to do what I
        // want here.
        return FALSE;
        break;
  }

  return $sql;
}


function _config_diff_table_compare($table_name) {
  global $db_type;

  // Be on the safe side with regards to SQL injection.
  $table_name = db_escape_table($table_name);

  // We need to decide if an "update" to the field's value
  // is required or whether a field needs to be added.

  $sql = FALSE;

  // Check to see if the table exists in the remote but not in the local.
  $local_table_exists = db_table_exists("{$table_name}");
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_SET);
  $remote_content_type_exists = db_table_exists("{$table_name}");
  _config_diff_remote_active_db(_CONFIG_DIFF_REMOTE_RST);

  if ($remote_content_type_exists && !$local_table_exists) {
    switch ($db_type) {
      case 'mysql':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_mysql.inc');
        $sql = _config_diff_table_compare_make_mysql($table_name);
        break;
      case 'mysqli':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_mysqli.inc');
        $sql = _config_diff_table_compare_make_mysqli($table_name);
        break;
      case 'pgsql':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_pgsql.inc');
        $sql = _config_diff_table_compare_make_pgsql($table_name);
        break;
    }
  }
  else {
    switch ($db_type) {
      case 'mysql':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_mysql.inc');
        $sql = _config_diff_table_compare_diff_mysql($table_name);
      case 'mysqli':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_mysqli.inc');
        $sql = _config_diff_table_compare_diff_mysqli($table_name);
        break;
      case 'pgsql':
        include_once(drupal_get_path('module', 'config_diff') .'/config_diff_pgsql.inc');
        $sql = _config_diff_table_compare_diff_pgsql($table_name);
        break;
    }
  }

  return $sql;
}

